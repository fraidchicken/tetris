from shapely.geometry import box, Polygon
from shapely.ops import unary_union
from shapely.affinity import scale, rotate, translate

import pygame
import copy
import random
import json

pygame.init()


class Mino():
    def __init__(self, name, coords, color, center=None):
        self.name = name
        boxes = [box(i * 20, j * 20, (i + 1) * 20, (j + 1) * 20)
                 for i, j in coords]
        self.mino = unary_union(boxes)
        self.color = color
        self.offset_x = 0
        self.offset_y = 0
        self.offset_angle = 0
        self.can_fall = True
        if center:
            self.center = center
        else:
            self.center = self.mino.centroid
        self.move(4, 0)

    def move(self, xoff, yoff):
        self.mino = translate(self.mino, xoff=xoff * 20, yoff=yoff * 20)
        self.center = translate(self.center, xoff=xoff * 20, yoff=yoff * 20)

    def rotate(self, angle):
        self.mino = rotate(self.mino, angle, origin=self.center)

    def __repr__(self):
        return self.name


class Cell():
    def __init__(self, x, y, area=0):
        self.x = x * 20
        self.y = y * 20
        self.box = box(self.x, self.y, self.x + 20, self.y + 20)
        self.area = area
        self._color = [0, 0, 0]
        self.fucked = False

    def move(self, xoff, yoff):
        self.box = translate(self.box, xoff=xoff * 20, yoff=yoff * 20)
        self.x += xoff * 20
        self.y += yoff * 20

    def test(self, poly):
        x = round(poly.mino.intersection(self.box).area / self.box.area, 1)
        return x + self.area <= 1

    def blend(self, poly):
        color = [0, 0, 0]
        for i in range(3):
            color[i] = self._color[i] ** 2 + poly.color[i] ** 2
            color[i] /= 2
            color[i] = color[i]**(1 / 2)

        return color

    def intersection(self, poly):
        area = round(poly.mino.intersection(self.box).area /
                     self.box.area, 1)
        self.area += area

        if area != 0:
            if self._color == [0, 0, 0]:
                self._color = poly.color
            else:
                self._color = self.blend(poly)

    def xy(self):
        return list(self.box.exterior.coords)

    def color(self):
        if self.area == 0:
            return (20, 20, 20)
        elif self.area > 1:
            self.fucked = True
            return self._color
        else:
            return (*self._color, self.area * 255)

    def __radd__(self, x):
        return self.area + x


class Core():
    def __init__(self, width, height, filename):
        self.width = width
        self.height = height
        self.mino = None
        self.threshold = .95
        self.queue = []
        self.hold_piece = None
        self.can_hold = True
        self.filename = filename
        self.clear()
        self.points = 0

    def clear(self, width=None, height=None):
        self.width = width or self.width
        self.height = height or self.height
        self.board = [
            [
                Cell(i, j, 1) if i == -
                1 or i == self.width or j == self.height else Cell(i, j)
                for i in range(-1, self.width + 1)
            ] for j in range(self.height + 1)
        ]

        self.minos = []

    def new_mino(self):
        self.can_hold = True
        if self.queue == []:
            self.queue = copy.deepcopy(self.minos)
            random.shuffle(self.queue)
        self.mino = self.queue.pop()

    def check(self, poly):
        for row in self.board:
            for cell in row:
                if cell.test(poly) == False:
                    return False
        return True

    def move(self, xoff, yoff, move_down=False):
        temp_mino = copy.deepcopy(self.mino)
        temp_mino.move(xoff, yoff)

        check = self.check(temp_mino)
        if check:
            self.mino = temp_mino
            self.mino.offset_x += xoff
            self.mino.offset_y += yoff
        elif move_down:
            self.mino.can_fall = False
        return check

    def rotate(self, angle):
        temp_mino = copy.deepcopy(self.mino)
        temp_mino.rotate(angle)

        check = self.check(temp_mino)
        if check:
            self.mino = temp_mino
            self.mino.offset_angle += angle
        return check

    def save(self):
        for row in self.board:
            for cell in row:
                cell.intersection(self.mino)

    def calc_points(self, row):
        if row == 4:
            self.points += 1000
        else:
            self.points += row * 100

    def clear_line(self):
        count = 0
        for i, row in enumerate(self.board[:-1]):
            x = sum([cell for cell in row[1:-1]])
            if x >= self.width * self.threshold:
                k = i

                while k != 0:
                    self.board[k] = self.board[k - 1]
                    for z in self.board[k]:
                        z.move(0, 1)
                    k -= 1

                self.board[0] = [
                    Cell(z, 0, 1) if z == -1 or z == self.width else Cell(z, 0)
                    for z in range(-1, self.width + 1)
                ]
                count += 1
        if count:
            self.calc_points(count)

    def hold(self):
        if self.can_hold == False:
            return
        self.mino, self.hold_piece = self.hold_piece, self.mino

        self.mino or self.new_mino()
        self.can_hold = False

    def load_pieces(self):
        with open(self.filename) as f:
            data = json.load(f)
            for name, values in data.items():
                if 'color' in values:
                    color = values['color']
                else:
                    color = [255, 255, 255]

                if 'center' in values:
                    x, y = values['center']
                    center = box(x * 20, y * 20, x * 20 +
                                 20, y * 20 + 20).centroid
                else:
                    center = None

                if 'blocks' in values:
                    blocks = values['blocks']
                else:
                    raise Exception('Blocks not found')

                self.minos.append(Mino(name, blocks, color, center))

    def reset(self):
        self.points = 0
        self.clear()
        self.load_pieces()
        self.new_mino()
        self.hold_piece = None


class Input():
    def __init__(self, string, xy=(0, 0), start=0, conversion=float):
        self.xy = xy
        self.screen_area = None
        offset = 10
        self._value = str(start)
        self.value = start
        self.conversion = conversion

        self.font = pygame.font.SysFont('Arial', 20)
        self.text = self.font.render(string, False, (255, 255, 255))

        self.area = pygame.Surface((150, 50))
        y_center = (self.area.get_height() - self.text.get_height()) // 2
        self.area.fill((20, 20, 20))
        self.area.blit(self.text, (offset, y_center))

        self.input_area = pygame.Surface(
            (150 - self.text.get_width() - offset * 3, self.text.get_height()))

        self.text_pos = (self.text.get_width() + offset * 2,
                         (self.area.get_height() - self.text.get_height()) // 2)
        self.update_text()

    def update_text(self):
        self.input_area.fill((255, 255, 255))
        text = self.font.render(self._value, False, (0, 0, 0))
        self.input_area.blit(text, (5, 0))
        self.area.blit(self.input_area, self.text_pos)

    def add(self, key):
        if key == pygame.K_BACKSPACE and self._value != '':
            self._value = self._value[:-1]
        elif pygame.key.name(key).isdigit() or pygame.key.name(key) == '.':
            self._value += pygame.key.name(key)

        self.update_text()

    def update_value(self):
        try:
            self.value = self.conversion(self._value)
        except ValueError:
            self.value = 0


def load_config():
    with open('config.json') as f:
        data = json.load(f)

        return data['core'], data['game']


core_cfg, game_cfg = load_config()


WIDTH, HEIGHT = game_cfg['width'], game_cfg['height']
game = Core(**core_cfg)
game.load_pieces()
game.new_mino()

screen = pygame.display.set_mode((WIDTH, HEIGHT))
playfield = pygame.Surface(
    (game.width * 20, game.height * 20), pygame.SRCALPHA)
hold_screen = pygame.Surface((20 * 4, 20 * 4), pygame.SRCALPHA)

inputs = {
    'offset': Input("offset", (100, 200), 1, float),
    'angle':  Input("rotation", (100, 300), 90, float),
    'threshold':  Input("threshold", (100, 400), 1, float)
}

clock = pygame.time.Clock()
piece_down = pygame.USEREVENT + 1
pygame.time.set_timer(piece_down, 250)

pygame.font.init()
font = pygame.font.SysFont('Arial', 30)

selected = None
running = True
pause = False
fucked = False
# hold_matrix = []

screen.fill((0, 0, 0))

while running:
    # event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if not pause:
            game.threshold = inputs['threshold'].value

        if event.type == pygame.MOUSEBUTTONDOWN and pause:
            if event.button == 1:
                for i in inputs.values():
                    if i.screen_area.collidepoint(pygame.mouse.get_pos()):
                        i._value = ''
                        i.update_value()
                        i.update_text()
                        selected = i

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                if pause:
                    for i in inputs.values():
                        i.update_value()
                pause = not pause
            elif pause:
                if selected:
                    selected.add(event.key)
                else:
                    continue
            elif fucked and event.key != pygame.K_r:
                pass
            elif event.key == pygame.K_LEFT:
                game.move(-inputs['offset'].value, 0)
            elif event.key == pygame.K_RIGHT:
                game.move(inputs['offset'].value, 0)
            elif event.key == pygame.K_DOWN:
                game.move(0, inputs['offset'].value, True)
            elif event.key == pygame.K_r:
                game.queue = []
                game.reset()
                fucked = False
                playfield = pygame.Surface(
                    (game.width * 20, game.height * 20), pygame.SRCALPHA)
            elif event.key == pygame.K_z:
                game.rotate(inputs['angle'].value)
            elif event.key == pygame.K_x:
                game.rotate(-inputs['angle'].value)
            elif event.key == pygame.K_a:
                game.rotate(inputs['angle'].value * 2)
            elif event.key == pygame.K_LSHIFT:
                game.hold()
                # hold_matrix = [[Cell(x, y) for x in range(4)]
                #                for y in range(4)]
                # for row in hold_matrix:
                #     for cell in row:
                #         cell.intersection(game.hold_piece)
            elif event.key == pygame.K_s:
                game.rotate(-inputs['angle'].value * 2)
            elif event.key == pygame.K_SPACE:
                while game.mino.can_fall:
                    game.move(0, inputs['offset'].value, True)
                game.save()
                game.new_mino()
            elif event.key == pygame.K_p:
                for i in game.board:
                    for j in i:
                        print(f'[{j.x:<3} {j.y:<3} {j.area:<3}]', end=' ')
                    print('')
                for j, i in inputs.items():
                    print(f'{j} {i.value}')
        if event.type == piece_down and not pause and not fucked:
            if game.mino.can_fall:
                game.move(0, inputs['offset'].value, True)
            elif game.move(0, inputs['offset'].value, True):
                game.mino.can_fall = True
            else:
                game.save()
                game.new_mino()

    game.clear_line()

    # render
    screen.fill((0, 0, 0))
    playfield.fill((0, 0, 0))
    # hold_screen.fill((20, 20, 20))

    render_matrix = copy.deepcopy(game.board)
    for row in render_matrix[:-1]:
        for cell in row[1:-1]:
            cell.intersection(game.mino)
            pygame.draw.polygon(playfield, cell.color(), cell.xy())
            fucked |= cell.fucked

    # if game.hold_piece:
    #     for row in hold_matrix:
    #         for cell in row:
    #             pygame.draw.polygon(hold_screen, cell.color(), cell.xy())

    # blit
    screen.blit(playfield, ((WIDTH - game.width * 20) // 2, 100))
    # screen.blit(hold_screen, ((WIDTH - game.width * 20) /
    #                           2 - 5 * 20, 100))

    for i in inputs.values():
        i.screen_area = screen.blit(i.area, i.xy)

    text_points = font.render(f'{game.points:0>9}', False, (255, 255, 255))
    text_rect_points = text_points.get_rect(center=(WIDTH//2, 50))
    screen.blit(text_points, text_rect_points)

    if pause:
        text = font.render('Pause', False, (255, 255, 255))
        text_rect = text.get_rect(center=(WIDTH//2, HEIGHT//2))
        screen.blit(text, text_rect)

    if fucked:
        text = font.render('Press R to restart', False, (255, 255, 255))
        text_rect = text.get_rect(center=(WIDTH//2, HEIGHT//3))
        screen.blit(text, text_rect)

    pygame.display.update()

    clock.tick(60)

pygame.quit()
